// Import dependencies
const app = require("express")();
const bodyParser = require("body-parser");
const passport = require("passport");
const UserModel = require("./models/user");

// Set port number
const PORT = process.env.PORT || 5000;

const user = require("./routes/api/user");
const profile = require("./routes/api/profile");

// Call DB connection
const connection = require("./config/dbConnect");

// enable parsing incoming data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Passport midddleware
app.use(passport.initialize());
require("./config/passport")(passport);

// Set routes
app.use("/api/user", user);
app.use("/api/profile", profile);

// SET a test router
app.get("/", (req, res) => {
  res.json({ msg: "success" });
});

// Start the server
app.listen(PORT, () => console.log(`The server is running at PORT ${PORT}`));
