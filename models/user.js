const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  first_name: {
    type: String,
    match: /[a-z]/,
    required: true
  },
  last_name: {
    type: String,
    match: /[a-z]/
  },
  email: {
    type: String,
    required: true
  },
  user_name: {
    type: String,
    required: true
  },
  role: {
    // Role 1 = 'talent' 0 = 'recruiter'
    type: Number,
    default: 1
  },
  password: {
    type: String,
    required: true
  },
  register_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("user", UserSchema);
