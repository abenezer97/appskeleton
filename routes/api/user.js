const router = require("express").Router();
const passport = require("passport");

// Import Register Controller
const userController = require("./../controllers/userController");

// @route GET api/user/test
// @desc test router
// @access public
router.get("/test", (req, res) => {
  res.json({ msg: "The user router is working." });
});

// @route POST api/user/register
// @desc Regsiter a user
// @access public
router.route("/register").post(userController.register);

// @route POST api/user/login
// @desc Login a user / return a token
// @access public
router.route("/login").post(userController.login);

// @route GET api/user/current
// @desc Return the current user
// @access private
router
  .route("/current")
  .get(
    passport.authenticate("jwt", { session: false }),
    userController.current
  );

module.exports = router;
