const router = require("express").Router();

// @route GET api/profile/test
// @desc profile route test
// @access public
router.get("/test", (req, res) => {
  res.json({ msg: "The profile route is working perfectly" });
});

module.exports = router;
