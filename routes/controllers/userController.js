const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("./../../models/user");

// Import secret key
const secretOrKey = require("../../config/keys").secretOrKey;

// Handle Signup
exports.register = (req, res) => {
  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      return res
        .status(400)
        .json({ error: "User already exit with this email" });
    } else {
      const newUser = new User({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        user_name: req.body.user_name,
        email: req.body.email,
        password: req.body.password
      });

      // hash password using bcrypt
      bcrypt.genSalt(10, (err, salt) => {
        //generatin a salt
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          //hash the given password with the generated salt
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then(user => res.json(user))
            .catch(err => console.log("ERROR", err));
        });
      });
    }
  });
};

// Handle Login
exports.login = (req, res) => {
  User.findOne({ user_name: req.body.user_name }).then(user => {
    if (!user) {
      return res.status(404).json({ err: "user not found" });
    } else {
      // Compare password using bcrypt.compare
      bcrypt.compare(req.body.password, user.password, (err, success) => {
        if (!success) {
          return res.status(400).json({ err: "password incorrect" });
        } else {
          // Creae jwt payload
          const payload = {
            id: user._id,
            first_name: user.first_name,
            last_name: user.last_name,
            role: user.role
          };
          // Sign token
          jwt.sign(payload, secretOrKey, { expiresIn: 3600 }, (err, token) => {
            if (token) {
              return res.json({ success: true, token: "Bearer " + token });
            }
          });
        }
      });
    }
  });
};

// Return Current User
exports.current = (req, res) => {
  const user = req.user;
  res.json({
    id: user.id,
    first_name: user.first_name,
    last_name: user.last_name,
    email: user.email,
    user_name: user.user_name,
    roel: user.role
  });
};
