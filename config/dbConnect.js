const mongoose = require("mongoose");
const mongoURI = require("./keys").mongoURI;

const connection = mongoose
  .connect(mongoURI, { useNewUrlParser: true })
  .then(() => console.log("connected with Database Successfully"))
  .catch(err => console.error("ERROR Failed to Connect with Database"));

module.exports = connection;
